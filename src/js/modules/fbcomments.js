export default function (id){
  const comments = /*html*/`
    <div class="fb-comments" data-href="${location.href}" data-numposts="5" data-width=""></div>
  `
  const container = document.getElementById(id)
  if(!container) return;
  addEventListener('DOMContentLoaded',()=>container.innerHTML=comments)
}
