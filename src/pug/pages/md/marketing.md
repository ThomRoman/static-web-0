# Herramientas para marketing

Protocos que utilizan facebook (Open Graph) y twitter (Twitter Card) para compartir contenido

- [Open Graph](https://ed.team/blog/que-es-open-graph-y-como-implementarlo)
  - [Documentation](https://ogp.me)
  - [Sharing debugger](https://developers.facebook.com/tools/debug/sharing/)
- [Twitter card](https://ed.team/blog/optimiza-tu-web-con-twitter-cards)
  - [Documentation](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started)
  - [Validator](https://cards-dev.twitter.com/validator)

## Insertar Twitter

- [Publish](https://publish.twitter.com)

- Collecciones
  - Collecciones personalizadas de twits
  - se crean solo desde `tweetdeck` (+ Add column - collection)
  - tutorial `https://developer.twitter.com/en/docs/tweets/curate-a-collection/overview/overview.html`

## Insertar Facebook comments

.

## SiteMap `sitemap.xml`

- [SiteMap I]([https://link](https://es.semrush.com/blog/crear-sitemap-web/?kw=&cmp=LA_SRCH_DSA_Blog_SEO_ES&label=dsa_blog&Network=g&Device=c&utm_content=434618320712&kwid=dsa-837315083898&cmpid=8044628370&gclid=CjwKCAjw8J32BRBCEiwApQEKgXxSXhzJ1_9yvy7PdiZf_aAd3n-XfrlB_RnAMwqWDmX8I77zUFZFRRoCpDQQAvD_BwE))
- [SiteMap II](https://juanluismora.es/seo/sitemap-para-un-dominio.html)

Es un archivo xml que se aloja en la url de tu sitio web, y aunque es pública , lo recomentable es que solo
lo lean los bots de los distintos buscadores(google, etc). Este archivo es un listado de las páginas que componen
un sitio web junto con alguna informacion adicional,tal como
cúando fue su última actualización y que importante es respecto
a las demas páginas . Ademas facilitan el descubriento y rastreo de páginas para los robots de los buscadores.

Este archivo solo le es util al buscador. Este archivo debería reflejar los últimos cambios que se han hecho en
los artículos de tu blog.

Las URLs incluidas en un sitemap deben corresponder a páginas actuales que consideramos importante indexar. Por eso
se deben mantener actualizados.

Eso es muy necesario para el SEO (Search Engine Optimization)

### Como usarlo de manera correcta

Los cambios deben presentar de orden cronoñogico inverso, es decir que el ultimo cambio del sitio debe colocarse primero en el `sitemap.xml`

Deben aparecer todos los cambios que se le hayan hecho en las URLs

Lo que hace los bots del motor de busqueda que usemos es solo seguir enlaces y, cuanto más fácil le ofrezcas el contenido nuevo, más aprovechara el tiempo en el sitio.

**Camino del Bots :**

- robots.txt : para ver que puede indexar y que no
  - El archivo “robots.txt” les dice a los motores de búsqueda a qué partes de tu sitio pueden acceder y
    consecuentemente rastrear.
- Nosotros en la ultima linea del robots.txt tenemos que colocar
  - el sitemap.xml
- Una ves el bot entra al sitemap.xml leera el primer URL que será el 
  - ultimo cambio que le hallamos hecho a nuestro sitio

### **Contenido obligatorio de los sitemaps**

````xml
  <?xml version="1.0" encoding="UTF-8"?>

  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"> <!-- esto hace referencia al protocolo de uso 0.9-->

    <url>

      <loc>http://www.example.com/pagina.htm</loc>

      <!-- Estas son etiquetas opcionales pero muy recomndables, mientras que las anteriores son obligatorias -->
      <lastmod>2009-12-01</lastmod>

      <changefreq>monthly</changefreq>

      <priority>0.5</priority>

    </url>

    <url>

        <loc>http://www.example.com/</loc> <!-- Dirección url de la página que forma parte del Sitemap -->

        <lastmod>2005-01-01</lastmod>

        <changefreq>monthly</changefreq>

        <priority>1.0</priority>

    </url>

  </urlset>
````

## Robots.txt

- [Robots.txt I](https://juanluismora.es/seo/robots-txt.html)
- [Robots.txt II](https://www.woorank.com/es/blog/agregar-sitemap-archivo-robots-txt)

````txt
  User-Agent: *
  Disallow: /estilo/plantilla.css
  Disallow: /recursos/
  Disallow: /capcha/
  Disallow: /sesiones/
  Disallow: /test/
  Disallow: /img/
  Disallow: /css/
  Sitemap: http://impresas.es/sitemap.xml
````

En primer lugar con User-Agent: *, especificamos el buscador al que afectará la regla, si ponemos asterisco hacemos referencia a todos los
buscadores, también se puede poner por ejemplo google para hacer referencia sólo a google.. Luego deshabilitamos el acceso a todas aquellas 
carpetas o ficheros que no queremos que sean accedidas y por último vamos a indicar donde está nuestro sitemap y  de esta forma todos los 
robots que pasen por es sitio podrán localizar fácilmente nuestro sitemap.

Robots.txt es un archivo de texto simple que se coloca en el directorio raíz de tu sitio. Es ese archivo en tu sitio web que le dice a estos robots de motores de búsqueda qué rastrear y qué no rastrear en tu sitio. También contiene comandos que describen qué robots de motores de búsqueda pueden rastrear y cuáles no.
